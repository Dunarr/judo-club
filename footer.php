<footer class="footer">
    <div class="footer__inner">
        <div class="footer__right">
            <span class="footer__right__link">Agrément Jeunesse et Sports n° <?= get_field('agrement', 'home-options') ?></span>
            <?php foreach(get_field('footer_menu', 'home-options') as $link): ?>
                            <a class="footer__right__link" href="<?= $link['link']['url'] ?>" target="<?= $link['link']['target'] ?>"><?= $link['link']['title'] ?></a>
            <?php endforeach; ?>
        </div>
        <?php foreach(get_field('liens_externes', 'home-options') as $link): ?>
            <a href="<?= $link["lien"] ?>" class="footer__link">
                <img src="<?= $link["logo"] ?>" alt="<?= $link["lien"] ?>">
            </a>
	    <?php endforeach; ?>
    </div>

</footer>
<?php
    wp_footer();
?>
</body>
</html>
