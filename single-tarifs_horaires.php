<?php
get_header();
if ( have_posts() ):
	while ( have_posts() ):
		the_post();
		?>
        <div class="container margin">
            <h1><?php the_title() ?></h1>
            <div class="col_12 wysiyg">
				<?php
				the_field( 'contenu_haut' );
				?>
                <h2>Tarifs</h2>
                <div class="table-stripped">
                    <table>
                        <thead>
                        <tr>
                            <th>Catégories</th>
                            <th>Licence</th>
                            <th>Année</th>
                            <th>Sargé/Yvré</th>
                            <th>Hors Sargé/Yvré</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php
						$tarifs = get_field( 'tarifs' );
						foreach ( $tarifs as $tarif ): ?>
                            <tr>
                                <td><?= $tarif['groupe'] ?></td>
                                <td><?= $tarif['age'] ?></td>
                                <td><?= $tarif['licence'] ?>€</td>
                                <td><?= $tarif['sarge_yvre'] ?>€</td>
                                <td><?= $tarif['hors_sarge_yvre'] ?>€</td>
                            </tr>
						<?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <h2>Horaires</h2>
                <div class="table-stripped">
                    <table>
                        <thead>
                        <tr>
                            <th>Catégories</th>
                            <th>Jours</th>
                            <th>Horaires</th>
                        </tr>
                        </thead>
                        <tbody>
						<?php
						$horaires = get_field( 'horaires' );
						foreach ( $horaires as $horaire ): ?>
                            <tr class="<?= empty($horaire['groupe'])?'no-border':'' ?>">
                                <td><?= $horaire['groupe'] ?></td>
                                <td><?= $horaire['jour'] ?></td>
                                <td><?= $horaire['horaires'] ?></td>
                            </tr>
						<?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
				<?php
				the_field( 'contenu_bas' );
				?>
            </div>
        </div>
	<?php
	endwhile;
endif;
get_footer();
?>
