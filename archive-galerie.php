<?php
get_header(); ?>

<div class="container margin">
    <h1>Les photos du club</h1>
    <div class="col_12">
		<?php
		if ( have_posts() ):
			while ( have_posts() ):
				the_post();
				?>
            <div class="col_6">
                <a class="picture-card " href="<?php the_permalink(); ?>">
                    <img src="<?= get_the_post_thumbnail_url(null, "medium_large") ?>" alt="" class="picture-card__thumb">
                    <h3 class="picture-card__title"><?= get_the_title() ?></h3>
                </a>
            </div>
				<?php
				the_excerpt();
			endwhile;
		endif;
		?>
    </div>
</div>
<?php

get_footer();
?>
