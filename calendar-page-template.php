<?php
/**
 * Template Name: Page calendrier
 * The template for displaying ACF flex content.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */


setlocale (LC_TIME, 'fr_FR.utf8','fra');

/** @var WP_Term[] $typeTerms */
$typeTerms = get_terms( 'event_type', array(
	'hide_empty' => true,
	'order' => 'DESC',
) );

/** @var WP_Term[] $typeTerms */
$publicTerms = get_terms( 'event_public', array(
	'hide_empty' => true,
	'order' => 'DESC',
) );

$type = isset($_GET["type"])?$_GET["type"]:null;
$public = isset($_GET["public"])?$_GET["public"]:null;

$tax_query = [];
if($type) {
    $tax_query[]=[
	    'taxonomy' => 'event_type',
	    'field'    => 'slug',
	    'terms'    => $type,
    ];
}
if($public) {
    $tax_query[]=[
	    'taxonomy' => 'event_public',
	    'field'    => 'slug',
	    'terms'    => $public,
    ];
}
if(sizeof($tax_query) > 1) {
    $tax_query['relation'] = "AND";
}

$args = array(
	'post_type'      => array( 'event' ),
	'posts_per_page' => -1,
	'meta_key'  => 'date',
	'orderby'   => 'meta_value_num',
	'order'     => 'ASC',
	'meta_query'	=> array(
		array(
			'key'		=> 'date',
			'value'		=> date('Ymd'),
			'compare'	=> '>='
		)
	)
);

if(sizeof($tax_query) >= 1) {
	$args['tax_query'] = $tax_query;
}
$query = new WP_Query( $args );
get_header();
if ( have_posts() ):
	while ( have_posts() ):
		the_post();
		?>
		<?php if ( ! empty( get_the_post_thumbnail_url() ) ): ?>
        <div class="home-banner" style="background-image: url(<?= get_the_post_thumbnail_url(null, "header_cover") ?>)">
        </div>
	<?php endif; ?>

        <div class="container  <?= empty( get_the_post_thumbnail_url() ) ? 'margin' : 'float' ?>">
            <h1><?php the_title() ?></h1>
                <form action="" class="calendar-form col_12">
                    <div class="col_5">
                        <select name="type" id="event-type" class="cc-select">
		                    <?php foreach($typeTerms as $term): ?>
                                <option value="<?= $term->slug ?>" <?= $term->slug===$type?"selected":'' ?>><?= $term->name ?></option>
		                    <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col_5">
                        <select name="public" id="event-public" class="cc-select">
		                    <?php foreach($publicTerms as $term): ?>
                                <option value="<?= $term->slug ?>" <?= $term->slug===$public?"selected":'' ?>><?= $term->name ?></option>
		                    <?php endforeach; ?>
                        </select>
                    </div>
                    <input type="submit" value="rechercher" class="col_2 search-button">
                </form>
            <div class="col_12">
            <?php
                    if($query->have_posts()):
                        while ($query->have_posts()):
                            $query->the_post();
	                        $date = new DateTime( get_field( "date" ) )
                    ?>
                            <a href="<?php the_permalink(); ?>" class="event-teaser <?= (new \DateTime())->diff($date)->format('%r%a') < 7?"event-teaser--soon":"" ?>">
                                <div class="event-teaser__date">
                                    <div class="event-teaser__date__year">
		                                <?= $date->format( "Y" ); ?>
                                    </div>
                                    <div class="event-teaser__date__day">
				                        <?= $date->format( "d/m" ); ?>
                                    </div>
                                </div>
                                <h3 class="event-teaser__title"><?php the_title(); ?></h3>
                            </a>
                    <?php
                endwhile;
                else:
                        ?>
                            <h2>Aucun évènement trouvé</h2>
                        <?php
                endif;
                wp_reset_postdata();
                ?>
            </div>
        </div>
	<?php
	endwhile;
endif;
get_footer();
?>
