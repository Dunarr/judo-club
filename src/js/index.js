import './Menu';
import './ContactMap';
import Lightbox from './Lightbox.ts';
import {DSelect} from "./DSelect.ts";

Lightbox.Init();
const selectType = document.getElementById('event-type')
if (selectType) {
  const ccSelectType = new DSelect(selectType)
  ccSelectType.title = "type"
  console.log(ccSelectType);
}


const selectPublic = document.getElementById('event-public')
if (selectPublic) {
  const ccselectPublic = new DSelect(selectPublic)
  ccselectPublic.title = "public"
}
