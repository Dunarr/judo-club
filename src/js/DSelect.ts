export class DSelect {
    public static instances: DSelect[] = [];
    private _root: HTMLSelectElement
    private _el:HTMLElement;
    private _options: DSelectOption[] = []
    private _multiple:boolean;
    private _label:HTMLElement;
    private _labelTitle:HTMLElement;
    private _labelArrow:HTMLElement;
    private _optionList:HTMLElement;
    private _opened: boolean;


    get opened() {
        return this._opened;
    }

    set opened(value) {
        this._opened = value;
        if(value) {
            this._labelArrow.classList.add("reversed")
            this._optionList.classList.add("opened")
        } else {
            this._labelArrow.classList.remove("reversed")
            this._optionList.classList.remove("opened")
        }
    }

    constructor(root: HTMLSelectElement, multiple:boolean = false) {
        this._root = root;
        this._multiple = multiple;
        this._label = document.createElement("div")
        this._labelTitle = document.createElement("span")
        this._labelArrow = document.createElement("div")
        this._label.classList.add("dselect__label");
        this._labelTitle.classList.add("dselect__label__title");
        this._labelArrow.classList.add("dselect__label__arrow");
        this._label.appendChild(this._labelTitle);
        this._label.appendChild(this._labelArrow);
        this._labelArrow.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>`
        this._el = document.createElement("div")
        this._el.classList.add("dselect");
        this._optionList = document.createElement("div")
        this._optionList.classList.add("dselect__option-list")
        this._el.appendChild(this._label)
        this._el.appendChild(this._optionList)
        this._root.parentElement.insertBefore(this._el, this._root)
        this._root.style.display = "none"
        this._root.classList.add("initialized")
        this._label.addEventListener("click", this.toggle)
        window.addEventListener("click", ({target})=> {
            if(!this._el.contains(<HTMLElement>target)){
                this.opened = false
            }
        })
        for (let i = 0; i < this._root.options.length; i++) {
            this.addOption(this._root.options[i])
        }
    }

    public toggle = () => {
        this.opened = !this.opened
    }

    public addOption(optionEl: HTMLOptionElement){
        const option = new DSelectOption(optionEl)
        this._options.push(option);
        this._optionList.appendChild(option.root);
        option.onChange(()=> {
            this.setCheckedOption(option)
        })
    }

    private setCheckedOption = (option: DSelectOption) => {
        if(!this._multiple) {
            if(option.checked) {
                this._options.forEach(_option => {
                    if (option !== _option) {
                        _option.checked = false;
                    }
                })
            }
        }
    }

    public static Init(selector: string) {
        const els = document.querySelectorAll(selector);
        for (let i = 0; i < els.length; i++) {
            if (!els[i].classList.contains("initialized")) {
                new DSelect(<HTMLSelectElement>els[i])
            }
        }
        return this.instances
    }

    get value():string|string[]|null {
        if(this._multiple) {
            return this._options.filter(option => option.checked).map(option => option.value)
        } else {
            const values = this._options.filter(option => option.checked).map(option => option.value)
            if(values.length > 0) {
                return values[0]
            } else {
                return null;
            }
        }
    }

    set title(value:string) {
        this._labelTitle.innerText = value;
    }
}

export class DSelectOption {
    private _value: string
    private _radio: HTMLElement;
    private _root: HTMLElement;
    private _optionEl: HTMLOptionElement;
    private _listeners: ((option: DSelectOption)=>void)[] = []
    private _checked: boolean = false;
    get checked(): boolean {
        return this._checked;
    }

    set checked(value: boolean) {
        const checked = this.checked
        this._optionEl.selected = value;
        this._checked = value
        if(value) {
            this._radio.classList.add("checked")
        } else {
            this._radio.classList.remove("checked")
        }
        if(checked !== value){
            this.notifyChange()
        }
    }

    notifyChange(){
        this._listeners.forEach(callback => {
            callback(this)
        })
    }

    onChange(callback: (option: DSelectOption)=>void) {
        this._listeners.push(callback)
    }

    get value(): string {
        return this._optionEl.value;
    }

    set value(value: string) {
        this._value = value;
    }

    get root(): HTMLElement {
        return this._root;
    }

    constructor(option: HTMLOptionElement) {
        this._optionEl = option;
        this._radio = document.createElement("div");
        this._radio.classList.add("dselect__option__radio")
        this._root = document.createElement("div")
        this._root.classList.add("dselect__option")
        const optionLabel = document.createElement("div")
        optionLabel.innerText = option.innerText
        optionLabel.classList.add("dselect__option__label")
        this.checked = !!option.selected;
        this._root.appendChild(optionLabel)
        this._root.appendChild(this._radio)
        this._root.addEventListener("click", () => {
            this.toggle()
        });
    }
    toggle = () => {
        this.checked = !this.checked
    }
}
