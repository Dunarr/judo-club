<?php
get_header(); ?>

<div class="container margin">
    <h1>Les derniers articles</h1>
    <div class="col_12 wysiyg">
		<?php
		if ( have_posts() ):
			while ( have_posts() ):
				the_post();
				?>
				    <h2><?php the_title() ?></h2>
				<?php
				the_excerpt();
			endwhile;
		endif;
		?>
    </div>
</div>
<?php

get_footer();
?>
