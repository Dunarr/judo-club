<?php
get_header(); ?>

<div class="container margin">
    <h1>Documents administratifs</h1>
    <div class="col_12">
		<?php
		if ( have_posts() ):
			while ( have_posts() ):
				the_post();
				?>
            <div class="col_3">
                <a target="_blank" href="<?php the_field("document"); ?>" class="file-download__card" <?= !empty(get_the_post_thumbnail_url())?get_the_post_thumbnail_url(null, "medium"):"" ?>>
                    <h3 class="file-download__title"><?php the_title() ?></h3>
                    <img src="<?= get_stylesheet_directory_uri() ?>/assets/download.svg" class="file-download__icon" alt="">
                </a>
            </div>
				<?php
				the_excerpt();
			endwhile;
		endif;
		?>
    </div>
</div>
<?php

get_footer();
?>
