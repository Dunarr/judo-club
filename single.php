<?php
get_header();
if ( have_posts() ):
	while ( have_posts() ):
		the_post();
		?>
		<?php if(!empty(get_the_post_thumbnail_url())): ?>
        <div class="home-banner" style="background-image: url(<?= get_the_post_thumbnail_url(null, "header_cover") ?>)">
        </div>
	<?php endif; ?>

        <div class="container  <?= empty(get_the_post_thumbnail_url())?'margin':'float' ?>">
            <h1><?php the_title() ?></h1>
            <div class="col_12 wysiyg">
				<?php the_content(); ?>
            </div>
        </div>
	<?php
	endwhile;
endif;
get_footer();
?>
