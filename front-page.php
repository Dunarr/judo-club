<?php
get_header();
if ( have_posts() ):
	while ( have_posts() ):
		the_post();

		$banners = get_field( 'bannieres' );
		$banner  = $banners[ array_rand( $banners ) ]['sizes']['header_cover']
		?>
        <div class="home-banner" style="background-image: url(<?= $banner ?>)">
        </div>

        <div class="container float">
            <h1>Judo club Sargé</h1>
	        <?php if ( ! empty( get_field( "message" )['text'] ) ): ?>
		        <?php if ( empty( get_field( "message" )['link'] ) ): ?>
                    <div class="indication-message wysiyg" style="background-color: <?= get_field( "message" )['color']; ?>">
                        <div class="indication-message__text">
	                        <?= get_field( "message" )['text']; ?>
                        </div>
                    </div>
		        <?php else: ?>
                    <a href="<?= get_field("message")['link'] ?>" class="indication-message wysiyg indication-message--link" style="background-color: <?= get_field( "message" )['color']; ?>">
                        <div class="indication-message__text">
		                    <?= get_field( "message" )['text']; ?>
                        </div>
                        <span class="indication-message__read-more">Voir plus</span>
                    </a>
		        <?php endif; ?>
	        <?php endif; ?>

            <div class="col_7">
                <h2>Les Prochains évènements <a href="<?php the_field('page-calendrier', 'home-options') ?>" class="link-read-more" >(tout voir)</a></h2>
                <?php
                $args = array(
	                'post_type'      => array( 'event' ),
	                'posts_per_page' => 3,
	                'meta_key'  => 'date',
	                'orderby'   => 'meta_value_num',
	                'order'     => 'ASC',
	                'meta_query'	=> array(
		                array(
			                'key'		=> 'date',
			                'value'		=> date('Ymd'),
			                'compare'	=> '>='
		                )
	                )
                );
                $query = new WP_Query( $args );
	            if($query->have_posts()):
		            while ($query->have_posts()):
			            $query->the_post();
			            $date = new DateTime( get_field( "date" ) )
			            ?>
                        <a href="<?php the_permalink(); ?>" class="event-teaser event-teaser--small">
                            <div class="event-teaser__date">
                                <div class="event-teaser__date__year">
						            <?= $date->format( "Y" ); ?>
                                </div>
                                <div class="event-teaser__date__day">
						            <?= $date->format( "d/m" ); ?>
                                </div>
                            </div>
                            <h3 class="event-teaser__title"><?php the_title(); ?></h3>
                        </a>
		            <?php
		            endwhile;
	            endif;
	            wp_reset_postdata();
	            ?>
                <h2>Les Actualités du club <a href="<?php the_field('page-actu', 'home-options') ?>" class="link-read-more" >(tout voir)</a></h2>
				<?php
				$args  = array(
					'posts_per_page' => 5
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ):
					while ( $query->have_posts() ):
						$query->the_post();
						$categories = wp_get_post_categories( get_the_ID() );
						if ( sizeof( $categories ) > 0 ) {
							$catIcon = get_field( 'icone', get_term( $categories[0] ) );
						} else {
							$catIcon = null;
						}
						?>
                        <a class="actu-card" href="<?php the_permalink(); ?>">
	                        <?php if ( $catIcon ): ?>
                                <img src="<?= $catIcon ?>" alt="" class="actu-card__icon">
	                        <?php endif; ?>
                            <div class="actu-card__content">
                                <h3><?php the_title() ?></h3>
                                <p><?php the_excerpt(); ?></p>
                            </div>

                        </a>
					<?php
					endwhile;
				endif;
				?>

            </div>
            <div class="col_5">
                <h2>Les dernières photos <a href="<?php the_field('page-galerie', 'home-options') ?>" class="link-read-more" >(tout voir)</a></h2>
				<?php
				$args  = array(
					'post_type'      => 'galerie',
					'posts_per_page' => 4
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ):
					while ( $query->have_posts() ):
						$query->the_post();
						?>
                        <a class="picture-card" href="<?php the_permalink(); ?>">
                            <img src="<?= get_the_post_thumbnail_url(null, "medium_large") ?>" alt="" class="picture-card__thumb">
                            <h3 class="picture-card__title"><?= get_the_title() ?></h3>
                        </a>
					<?php
					endwhile;
				endif;
				wp_reset_postdata();
				?>
            </div>

        </div>

        <div class="container">
			<?php foreach ( get_field( 'presentations' ) as $presentation ): ?>
                <div class="col_6 presentation">
                    <img src="<?= $presentation['image']['sizes']['medium_large'] ?>" class="presentation__thumb">
                    <h2 class="presentation__title"><?= $presentation['title'] ?></h2>
                    <p><?= $presentation['description'] ?></p>
                </div>
			<?php endforeach; ?>
        </div>
        <div class="container-invisible">
            <div class="wide-row flex-center">
			<?php
			$args  = array(
				'post_type'      => 'cour',
				'posts_per_page' => - 1
			);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ):
				while ( $query->have_posts() ):
					$query->the_post();
					?>
                    <a class="cours-card col_4" href="<?php the_permalink(); ?>">
                        <img src="<?= get_the_post_thumbnail_url(null, "medium_large") ?>" alt="" class="cours-card__thumb">
                        <div class="cours-card__title"><h3><?= get_the_title() ?></h3>
                            <h4><?= get_field( 'informations' ) ?></h4></div>

                    </a>
				<?php
				endwhile;
			endif;
			?>
            </div>
        </div>
        <div class="container-invisible">
            <div class="wide-row">
                <div class="col_6 footer-card container-card">
                    <h2>Nous contacter</h2>
                    <div class="contact-info">
                        <div class="contact-info__title">email:</div>
                        <div class="contact-info__contact">********@free.fr</div>
                    </div>
                    <div class="contact-info">
                        <div class="contact-info__title">tel:</div>
                        <div class="contact-info__contact">02 43 76 89 85</div>
                    </div>
                    <div class="contact-info">
                        <div class="contact-info__title">adresse:</div>
                        <div class="contact-info__contact">20 Rue Principale, 72190 Sargé-lès-le-Mans</div>
                    </div>
                </div>
                <div class="col_6 footer-card">
                    <div id="contact-map"></div></div>
            </div>
        </div>
	<?php
	endwhile;
endif;
get_footer();
?>
