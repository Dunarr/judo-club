<!doctype html>
<html lang="fr"class="<?= esc_attr(get_the_title()) ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Judo Club Sargé</title>
    <link rel="icon" href="<?= get_stylesheet_directory_uri() ?>/assets/favicon.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <?php
        wp_head();
    ?>
</head>
<body>

<nav class="site-nav">
        <a href="/" class="site-nav__logo">
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/Logo-sarge-2.png" alt="">
        </a>
        <a href="/" class="site-nav__logo site-nav__logo--mobile">
            <img src="<?= get_stylesheet_directory_uri() ?>/assets/Logo-sarge-mobile.png" alt="">
        </a>
        <div class="site-nav__toggle" id="menu-toggle">
            <div class="site-nav__toggle__bar site-nav__toggle__1"></div>
            <div class="site-nav__toggle__bar site-nav__toggle__2"></div>
            <div class="site-nav__toggle__bar site-nav__toggle__3"></div>
        </div>
        <?php
        wp_nav_menu(["theme_location"=>"primary", "menu_class"=>"site-nav__menu", "menu_id"=>"menu", "container_class"=>"site-nav__container"])
        ?>
</nav>

