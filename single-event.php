<?php
get_header();
if ( have_posts() ):
	while ( have_posts() ):
		the_post();
		?>
		<?php if ( ! empty( get_the_post_thumbnail_url() ) ): ?>
        <div class="home-banner" style="background-image: url(<?= get_the_post_thumbnail_url(null, "header_cover") ?>)">
        </div>
	<?php endif; ?>

        <div class="container  <?= empty( get_the_post_thumbnail_url() ) ? 'margin' : 'float' ?>">
            <h1><?php the_title() ?></h1>
            <div>
                <div class="col_2 event__data"><img src="<?= get_stylesheet_directory_uri() ?>/assets/calendar.svg" alt="date" class="event__data__icon"><?= (new DateTime( get_field( "date" ) ))->format("d/m/Y"); ?></div>
                <div class="col_2 event__data"><img src="<?= get_stylesheet_directory_uri() ?>/assets/clock.svg" alt="heure" class="event__data__icon"><?php the_field("debut"); ?><?= (!empty(get_field("debut"))&&!empty(get_field("fin")))?"-":""; ?><?php the_field("fin"); ?></div>
                <div class="col_8 event__data"><img src="<?= get_stylesheet_directory_uri() ?>/assets/map-pin.svg" alt="lieu" class="event__data__icon"><?php the_field("lieu"); ?></div>
            </div>
            <div class="col_12 event__description"><?php the_field( "description" ); ?></div>

        </div>
	<?php
	endwhile;
endif;
get_footer();
?>
