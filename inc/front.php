<?php

function pagination($pages = '', $range = 4)
{
	$showitems = ($range * 2)+1;

	global $paged;
	if(empty($paged)) $paged = 1;

	if($pages == '')
	{
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages)
		{
			$pages = 1;
		}
	}

	if(1 != $pages)
	{
		if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."' class='pagination__prev'>&lsaquo;</a>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."' class=\"pagination__link\">1</a>";
		if($paged > 3 && $paged > $range+1 && $showitems < $pages) echo "...";

		for ($i=1; $i <= $pages; $i++)
		{
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
			{
				echo ($paged == $i)? "<span class=\"pagination__link pagination__link--active\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"pagination__link\">".$i."</a>";
			}
		}

		if ($paged < $pages-2 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "...";
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."' class=\"pagination__link\">".$pages."</a>";
		if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\" class='pagination__next'>&rsaquo;</a>";
		echo "</div>\n";
	}
}
